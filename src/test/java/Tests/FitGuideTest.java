package Tests;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.TestBase;
import PageObjects.ProductDetailPage;
import Utils.SizeWidthValidator;
import Utils.TakeScreenshot;


public class FitGuideTest extends TestBase {
	

	 private ProductDetailPage page;
	 private SizeWidthValidator validator;
	 private static final Logger logger = LogManager.getLogger(FitGuideTest.class);
	 
	    @BeforeMethod
	    public void initialize() {
	        //Initialize
	        this.page = new ProductDetailPage(driver);
	        this.validator = new SizeWidthValidator();
	    }
	
	@Test
	public void testFitFuideFeature() throws Exception {
		try {
		logger.info("Test Method started");
		driver.get("https://www.birkenstock.com/gb/arizona-birko-flor-nubuck/arizona-core-birkoflornubuck-0-eva-u_650.html");
		
		page.confirmSelectionCookies();
	
		page.clickFitGuide();
	
		page.selectLeftFootLength("220 mm");
		page.selectLeftFootWidth("85 mm");
		page.selectRightFootLength("220 mm");
		page.selectRightFootWidth("85 mm");
		page.clickOnCalculateSize();
		String actualResult = page.getresultedSizeAndWidth();
		
		List<String> expectedResult = validator.validateSizeWidth(220, 85);
		String size = expectedResult.get(0);
		String width = expectedResult.get(1);
		
		assertTrue(actualResult.contains(size),"Expected size not found in the result");
		assertTrue(actualResult.contains(width),"Expected width not found in the result");
		
		page.clickOnAddToCart();
		TakeScreenshot.takeScreenshot(driver, "./test-screenshots/screenshots");

		logger.info("Test Method finished");
		}catch (Exception e) {
            logger.error("An error occurred in Test Method", e);
            throw e; 
        }
		
	}

}
