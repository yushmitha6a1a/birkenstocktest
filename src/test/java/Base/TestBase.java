package Base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class TestBase {
	
	protected WebDriver driver;
	
	@BeforeClass
	@Parameters({"browser"})
	public void setUp(String browser) throws Exception {
		if(browser.equalsIgnoreCase("chrome")) {
			 System.setProperty("webdriver.chrome.driver",getGlobalValue("chromePath"));
			  driver =  new ChromeDriver(); 	 
		}
		else if(browser.equalsIgnoreCase("firefox")) {
			 System.setProperty("webdriver.gecko.driver",getGlobalValue("geckoPath"));          
			 driver =  new FirefoxDriver(); 
		}else if (browser.equalsIgnoreCase("edge")) {
			System.setProperty("webdriver.edge.driver",getGlobalValue("edgePath"));
            driver = new EdgeDriver();
        } else {
            throw new Exception("Browser is not correct");
        }
		
		driver.manage().window().maximize();
		
	}
	
	@AfterMethod
	public void tearDown() {
		
			driver.quit();
		
		
	}
	
	
	public String getGlobalValue(String key) throws IOException {
		
		Properties prop = new Properties();
		FileInputStream stream = new FileInputStream("./src/test/resources\\selenium.properties");
		prop.load(stream);
		return prop.getProperty(key);
		
	}

}
