package PageObjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductDetailPage {
	
	private WebDriver driver;
	
	 // Locator for the "Fit Guide Fly in" element
    private By fitGuideLocator = By.xpath("//span[contains(text(),'Find your perfect fit')]");
    private By cookieButton = By.xpath("//div[contains(text(),'Select all')]");
    private By adBoxClose = By.xpath("//span[contains(text(),'close email sign up dialog')]");
    private By leftFootLengthLocator = By.xpath("//select[@id='length-select-left']");
    private By leftFootWidthLocator = By.xpath("//select[@id='width-select-left']");
    private By rightFootLengthLocator = By.xpath("//select[@id='length-select-right']");
    private By rightFootWidthLocator = By.xpath("//select[@id='width-select-right']");
    private By calculateSizeLocator = By.xpath("//button[contains(text(),'Calculate Size')]");
    private By confirmSelectionCookie = By.xpath("//div[contains(text(),'Confirm selection')]");
    private By resultedSize = By.xpath("//div[@class='result']");
    private By resultedWidth = By.xpath("//div[@class='recommended-width']");
    private By addToCartLocator = By.xpath("//div[@aria-label='Flyout menu']/descendant::button[@id='add-to-cart']");
	
	public ProductDetailPage(WebDriver driver) {
		this.driver =  driver;
	}
	
	public void clickFitGuide() {
		
		//WebElement fitGuideElement = driver.findElement(fitGuideLocator);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		wait.until(ExpectedConditions.presenceOfElementLocated(fitGuideLocator));
		WebElement fitGuideElement = driver.findElement(fitGuideLocator);
		try {
		fitGuideElement.click();
		}catch(StaleElementReferenceException e) {
			WebElement fitGuideElement2 = driver.findElement(fitGuideLocator);
			fitGuideElement2.click();
		}
		
	}
	
	public void closeAdBox() {
		WebElement adBoxCloseButton = driver.findElement(adBoxClose);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		wait.until(ExpectedConditions.visibilityOf(adBoxCloseButton));
		adBoxCloseButton.click();
	}
	
	public void acceptCookies() {
		WebElement selectAllCookies = driver.findElement(cookieButton);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		wait.until(ExpectedConditions.visibilityOf(selectAllCookies));
		selectAllCookies.click();
	}
	
	public void confirmSelectionCookies() {
		WebElement confirmSelectionCookies = driver.findElement(confirmSelectionCookie);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		wait.until(ExpectedConditions.visibilityOf(confirmSelectionCookies));
		confirmSelectionCookies.click();
	}
	
	public void selectLeftFootLength(String leftLength) {		
		//WebElement leftFootLength = driver.findElement(leftFootLengthLocator);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		WebElement leftFootLength = wait.until(ExpectedConditions.presenceOfElementLocated(leftFootLengthLocator));
		Select leftFootLegthSelector = new Select(leftFootLength);
		leftFootLegthSelector.selectByVisibleText(leftLength);
	}
	
	public void selectLeftFootWidth(String leftWidth) {
		WebElement leftFootWidth = driver.findElement(leftFootWidthLocator);
			Select leftFootLegthSelector = new Select(leftFootWidth);
			leftFootLegthSelector.selectByVisibleText(leftWidth);
	}
	
	public void selectRightFootLength(String rightLength) {
		WebElement rightFootLength = driver.findElement(rightFootLengthLocator);
			Select leftFootLegthSelector = new Select(rightFootLength);
			leftFootLegthSelector.selectByVisibleText(rightLength);
	}
	
	public void selectRightFootWidth(String rightWidth) {
		WebElement rightFootWidth = driver.findElement(rightFootWidthLocator);
			Select leftFootLegthSelector = new Select(rightFootWidth);
			leftFootLegthSelector.selectByVisibleText(rightWidth);
	}
	
	public void clickOnCalculateSize() {
		WebElement calculateSize = driver.findElement(calculateSizeLocator);
		calculateSize.click();
		
	}
	
	public String getresultedSizeAndWidth() {
		WebElement resultSize = driver.findElement(resultedSize);
		String text1 = resultSize.getText();
		WebElement resultWidth = driver.findElement(resultedWidth);
		String text2 = resultWidth.getText();
		String result =  text1 +text2;
		String result2 = result.replaceAll("\n", "").replace("Width:", "").replace("UK", "");
		return result2;
	}
	
	public void clickOnAddToCart() throws InterruptedException {
	
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		WebElement addToCartElement= wait.until(ExpectedConditions.elementToBeClickable(addToCartLocator));
		addToCartElement.click();
		Thread.sleep(2000);
	}

}
