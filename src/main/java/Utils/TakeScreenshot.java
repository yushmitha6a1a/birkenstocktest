package Utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class TakeScreenshot {
	
	public static void takeScreenshot(WebDriver driver, String filePath) throws IOException {
		TakesScreenshot shot = ((TakesScreenshot)driver);
		File scrfile = shot.getScreenshotAs(OutputType.FILE);
		File desfile = new File(filePath);
		FileUtils.copyFile(scrfile,desfile);
				
	}

}
