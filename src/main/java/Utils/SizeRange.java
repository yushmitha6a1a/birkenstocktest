package Utils;

public class SizeRange {


	
	    String size;
	    String widthType;
	    int lengthFrom;
	    int lengthTo;
	    int widthFrom;
	    int widthTo;

	    public SizeRange(String size, String widthType, int lengthFrom, int lengthTo, int widthFrom, int widthTo) {
	        this.size = size;
	        this.widthType = widthType;
	        this.lengthFrom = lengthFrom;
	        this.lengthTo = lengthTo;
	        this.widthFrom = widthFrom;
	        this.widthTo = widthTo;
	    }

	    public boolean fits(int length, int width) {
	        return length >= lengthFrom && length <= lengthTo && width >= widthFrom && width <= widthTo;
	    }
	}




