package Utils;

import java.util.ArrayList;
import java.util.List;

public class SizeWidthValidator {
	
	

	    public  List<String> validateSizeWidth(int lengthToCheck, int widthToCheck) {
	    	
	        List<SizeRange> sizes = new ArrayList<>();
	        sizes.add(new SizeRange("2,5", "Narrow", 216, 220, 81, 84));
	        sizes.add(new SizeRange("2,5", "Regular", 218, 225, 85, 89));
	        sizes.add(new SizeRange("3,5", "Narrow", 221, 226, 83, 86));
	        sizes.add(new SizeRange("3,5", "Regular", 226, 230, 87, 91));
	        sizes.add(new SizeRange("4,5", "Narrow", 227, 232, 84, 89));
	        sizes.add(new SizeRange("4,5", "Regular", 231, 234, 90, 98));
	        sizes.add(new SizeRange("5", "Narrow", 233, 240, 86, 91));
	        sizes.add(new SizeRange("5", "Regular", 235, 242, 92, 100));
	        sizes.add(new SizeRange("5,5", "Narrow", 241, 245, 88, 93));
	        sizes.add(new SizeRange("5,5", "Regular", 243, 247, 94, 102));
	        sizes.add(new SizeRange("7", "Narrow", 246, 252, 90, 95));
	        sizes.add(new SizeRange("7", "Regular", 248, 254, 96, 104));


	        String matchedSize = "Not found";
	        String matchedWidthType = "Not found";
	        List<String> sizeWidth = null;

	        for (SizeRange size : sizes) {
	            if (size.fits(lengthToCheck, widthToCheck)) {
	                matchedSize = size.size;
	                matchedWidthType = size.widthType;
	                sizeWidth = new ArrayList();
	                sizeWidth.add(matchedSize);
	                sizeWidth.add(matchedWidthType);
	                break;
	            }
	            
	        }
	        
	        return sizeWidth;

	    }
	}


